#!/usr/bin/env python

from app import app
from waitress import serve
import os

serve(app, port=os.environ['APP_PORT'])
