from flask_admin.contrib.pymongo import ModelView, filters
from wtforms import form, fields
from flask_admin.model.fields import InlineFormField, InlineFieldList

class InnerForm(form.Form):
    # name = fields.StringField('action')
    name = fields.StringField('_id')
    active = fields.StringField('Active')

class UserForm(form.Form):
    name = fields.StringField('_id')
    active = fields.StringField('Active')

    # Inner form
    inner = InlineFormField(InnerForm)

    # Form list
    form_list = InlineFieldList(InlineFormField(InnerForm))


class UserView(ModelView):
    column_list = ('_id', 'active')
    column_sortable_list = ('_id', 'active')
    column_filters = (filters.FilterLike('_id', 'Email'),
                      filters.FilterEqual('_id', 'Email'),
                      filters.FilterNotEqual('_id', 'Email'),
                      filters.FilterNotLike('_id', 'Email'))

    form = UserForm