import json
from bson import json_util
from wtforms import form, fields
from flask_admin.contrib.pymongo import ModelView, filters

class SyncJobForm(form.Form):
    user_id = fields.StringField('Owner')
    action = fields.StringField('Action')
    bank = fields.StringField('Bank')
    status = fields.StringField('Last Status')
    status = fields.StringField('Status Data')


class SyncJobView(ModelView):
    column_list = ('owner', 'action', 'bank', 'last_status', 'date_started', 'error', 'data')
    column_sortable_list = ('owner', 'action', 'bank', 'last_status', 'date_started')

    column_filters = (filters.FilterNotEqual('owner', 'Owner'),
                      filters.FilterEqual('owner', 'Owner'),
                      filters.FilterLike('owner', 'Owner'),
                      filters.FilterNotLike('owner', 'Owner'),
                      filters.FilterNotEqual('bank', 'Bank'),
                      filters.FilterEqual('bank', 'Bank'))
    can_delete = False

    form = SyncJobForm

    def get_list(self, *args, **kwargs):
        count, data = super(SyncJobView, self).get_list(*args, **kwargs)

        for item in data:
            item['data'] = json.dumps(item['status_data'], default=json_util.default)
            item['error'] = json.dumps(item['errors'], default=json_util.default)

        return count, data
