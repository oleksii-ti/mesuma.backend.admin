import sys, os
from flask import Flask
from flask_admin import Admin
from pymongo import MongoClient
from forms.user import UserView
from forms.sync_job import SyncJobView
from lib.mesuma_backend.config import Config
from lib.mesuma_backend.connectors import logging

config = Config()
log = logging.get()

client = MongoClient(config.mongo.host, config.mongo.port)
if config.mongo.get('user') and config.mongo.get('password'):
    try:
        client.authenticate(config.mongo.user, config.mongo.password, mechanism='SCRAM-SHA-1')
    except:
        e = sys.exc_info()[0]
        log and log.error('Can not authenticate to mongo({}) with user({}). Error: {}'.format(config.host, config.user, e))
        raise e

db = client[config.mongo.db]

app = Flask(__name__)

admin = Admin(app, name=os.environ['PROJECT']+"."+os.environ['ENV'], template_mode='bootstrap3')

admin.add_view(SyncJobView(db.sync_job, 'Sync Jobs'))
admin.add_view(UserView(db.user, 'Users'))
app.run()
